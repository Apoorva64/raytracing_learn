//
// Created by appad on 1/1/2022.
//

#ifndef RAYTRACING_MAIN_H
#define RAYTRACING_MAIN_H

#include<SDL.h>
#include <iostream>
#include "../Raytracer/Raytracer.h"

class Main {
public:
    int width, height;
    double target_fps{};
    SDL_RendererInfo renderer_info{};
    SDL_Window *window;
    SDL_Renderer *renderer;
    Raytracer *raytracer;
    SDL_Event events{};

    Main(int width, int height);


    void start();


    ~Main();

};


#endif //RAYTRACING_MAIN_H
