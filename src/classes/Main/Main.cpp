//
// Created by appad on 1/1/2022.
//

#include "Main.h"
#include"../../helper.h"

Main::Main(int width, int height) {
    SDL_Init(SDL_INIT_EVERYTHING);
    this->width = width;
    this->height = height;
    this->window = SDL_CreateWindow("title", 0, 0, 1920, 1080, 0);
    this->renderer = SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED);

    SDL_GetRendererInfo(this->renderer, &this->renderer_info);
    std::cout << "Renderer name: " << this->renderer_info.name << std::endl;
    std::cout << "Texture formats: " << std::endl;
    for (Uint32 i = 0; i < this->renderer_info.num_texture_formats; i++) {
        std::cout << SDL_GetPixelFormatName(this->renderer_info.texture_formats[i]) << std::endl;
    }
    this->raytracer = new Raytracer(this->width, this->height, this->renderer);


}


void Main::start() {
    bool running = true;
    this->raytracer->render();

    while (running) {
//            SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderClear(renderer);

        while (SDL_PollEvent(&this->events)) {
            if ((SDL_QUIT == events.type) ||
                (SDL_KEYDOWN == events.type && SDL_SCANCODE_ESCAPE == events.key.keysym.scancode)) {
                running = false;
                break;
            }
            switch (events.key.keysym.sym) {
                case SDLK_LEFT:
                    raytracer->render();
                    break;
                case SDLK_s: {
                    save_texture("screenshot.bmp", renderer, raytracer->texture);
                }
                default:
                    break;
            }
        }
        this->raytracer->update();
        this->raytracer->draw();
        SDL_RenderPresent(renderer);
        SDL_Delay(16);
    }
}

Main::~Main() {
    SDL_DestroyRenderer(this->renderer);
    SDL_DestroyWindow(this->window);
    delete this->raytracer;
}
