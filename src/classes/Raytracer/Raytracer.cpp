

//
// Created by appad on 1/2/2022.
//

#include "Raytracer.h"
#include "fractals.h"
#include <future>
#include <chrono>
#include"../../helper.h"

#define THREAD_COUNT 14


Raytracer::Raytracer(int width, int height, SDL_Renderer *renderer) {
    this->width = width;
    this->height = height;
    this->renderer = renderer;
    this->texture = SDL_CreateTexture(this->renderer, SDL_PIXELFORMAT_ARGB8888,
                                      SDL_TEXTUREACCESS_STREAMING, this->width, this->height);
    this->pixels = new std::vector<unsigned char>(width * height * 4, 0);

    this->done = false;


}


void Raytracer::render() {
//    futures.clear();
    int height_part = height / THREAD_COUNT;
    int progress = 0;
    for (int i = 0; i < THREAD_COUNT; ++i) {
        this->futures.push_back(
                std::async(std::launch::async, mandelbrot, pixels, width, progress + height_part, 0, progress, center,
                           width, height));
        progress += height_part;
    }
}


void Raytracer::update() {

    if (!done) {
        done = true;
        for (const auto &future: futures) {
            using namespace std::chrono_literals;
            if (future.wait_for(0s) != std::future_status::ready) {
                done = false;
            }
        }
        unsigned char *lockedPixels = nullptr;
        int pitch = 0;
        SDL_LockTexture
                (
                        this->texture,
                        nullptr,
                        reinterpret_cast< void ** >( &lockedPixels ),
                        &pitch
                );
        std::memcpy(lockedPixels, pixels->data(), pixels->size());
        SDL_UnlockTexture(this->texture);
        if (done) {
            SDL_Log("done");
            save_texture("done.bmp", renderer, texture);

        }
    }
}

void Raytracer::draw() const {
    SDL_RenderCopy(this->renderer, this->texture, nullptr, nullptr);

}

Raytracer::~Raytracer() {
//        SDL_DestroyTexture(this->texture);
    delete this->pixels;

}
