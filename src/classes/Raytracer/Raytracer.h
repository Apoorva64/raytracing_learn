//
// Created by appad on 1/2/2022.
//

#ifndef RAYTRACING_RAYTRACER_H
#define RAYTRACING_RAYTRACER_H

#define PIXELS_PER_FRAME 3000

#include <SDL.h>
#include<vector>
#include <iostream>
#include<glm/glm.hpp>
#include<thread>
#include <future>

class Raytracer {
public:
    int width;
    int height;
    SDL_Renderer *renderer;
    SDL_Texture *texture;
    bool done;
    std::vector<unsigned char> *pixels;

    Raytracer(int width, int height, SDL_Renderer *renderer);


    void update();

    void draw() const;

    ~Raytracer();
    void render();


private:
//    glm::dvec2 center = {0.47, 0.639};
    glm::dvec2 center = {0.3, 0.639};
//    glm::dvec2 center = {0.5, 0.5};
    std::vector<std::future<void>> futures;

};


#endif //RAYTRACING_RAYTRACER_H
