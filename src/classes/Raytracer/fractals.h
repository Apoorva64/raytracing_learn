//
// Created by appad on 1/4/2022.
//



#ifndef RAYTRACING_FRACTALS_H
#define RAYTRACING_FRACTALS_H
#define iter_maldalbrot 50000


void mandelbrot(std::vector<unsigned char> *pixels, int end_x, int end_y, int start_x, int start_y,
                glm::dvec2 center,int width,int height) {
    std::cout << start_y << " " << end_y << std::endl;
    for (int y = start_y; y < end_y; y++) {
        for (int x = start_x; x < end_x; x++) {
            glm::dvec2 z, c;
            double scale =0.1;
            double brightness = 350;


            c.x = 1.3333 * ((double) x / width - 0.5) * scale - center.x;
            c.y = ((double) y / height - 0.5) * scale - center.y;

            int i;
            z = c;
            for (i = 0; i < iter_maldalbrot; i++) {
                double x = (z.x * z.x - z.y * z.y) + c.x;
                double y = (z.y * z.x + z.x * z.y) + c.y;

                if ((x * x + y * y) > 4.0) break;
                z.x = x;
                z.y = y;
            }

            int ir = static_cast<int>(37 * brightness * i / iter_maldalbrot);
            int ig = static_cast<int>(0* brightness * i / iter_maldalbrot);
            int ib = static_cast<int>(255 * brightness * i / iter_maldalbrot);
            const unsigned int offset = (end_x * 4 * y) + x * 4;
            pixels->at(offset + 0) = ir;
            pixels->at(offset + 1) = ig;      // g
            pixels->at(offset + 2) = ib;     // r
            pixels->at(offset + 3) = SDL_ALPHA_OPAQUE;    // a
        }
    }
}

#endif