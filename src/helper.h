//
// Created by appad on 1/6/2022.
//
#pragma once
#ifndef RAYTRACING_HELPER_H
#define RAYTRACING_HELPER_H
#include<SDL.h>

void save_texture(const char *filename, SDL_Renderer *ren, SDL_Texture *tex);

#endif //RAYTRACING_HELPER_H
