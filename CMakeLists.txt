cmake_minimum_required(VERSION 3.21)
project(raytracing)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_TOOLCHAIN_FILE "DCMAKE_TOOLCHAIN_FILE=C:/Users/appad/Documents/vcpkg/scripts/buildsystems/vcpkg.cmake")
#set(CMAKE_BUILD_TYPE Relase)
find_package(SDL2 CONFIG REQUIRED)
find_package(sdl2-image CONFIG REQUIRED)

#add_executable(raytracing src/main.cpp classes/Sphere.cpp classes/Sphere.h classes/Ray.cpp classes/Ray.h)
add_executable(raytracing src/main.cpp src/classes/Main/Main.cpp src/classes/Main/Main.h src/classes/Raytracer/Raytracer.cpp src/classes/Raytracer/Raytracer.h src/classes/Raytracer/fractals.h src/helper.h src/helper.cpp)
target_link_libraries(raytracing PRIVATE SDL2::SDL2 SDL2::SDL2main)
target_link_libraries(raytracing PRIVATE SDL2::SDL2_image)
